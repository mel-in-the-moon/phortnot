<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/head.php';

include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/user.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/connect.php';

$variable = "forminscription";

if(isset($_POST["nom"])){
    $nom = $_POST["nom"];

}

?>

<header>
    <div class="container">
    <h1> <a href="index.php"> PhortNot</a></h1>
    <nav class="nav-wrap">
                    <ul class="group" id="menu">
                    <li class="current_page_item_two"><a rel="/" href="/">ACCEUIL</a></li>
                    
                    <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
                    <li><a rel="#fe4902" href="profil.php">profil</a></li>
                    <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                    <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                    <li><a rel="#D40229" href="#">STATS</a></li>
                    <li><a rel="#98CEAA" href="blog.php">BLOG</a></li>
            
            </ul>
            </nav>

        
    </div>
</header>
<section class="text">
    <?php include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/content/main.php'; ?>
</section>
<section class="form">
    <?php
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/forms/inscription.php';
    include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/forms/connexion.php';
    ?>
</section>

<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';
