<?php
session_start();

require_once 'functions/config.php';

if(!isset($_SESSION['admin']) OR !$_SESSION['admin']) {
	header('Location: blog.php');
}

$message_categorie = '';
if(isset($_POST['categorie'])) {
	if(!empty($_POST['nom']) AND !empty($_POST['slug'])) {
		$nom = htmlspecialchars($_POST['nom']);
		$slug = htmlspecialchars($_POST['slug']);

		$ins = $bdd->prepare('INSERT INTO categories (categorie, categorie_url) VALUES (?, ?)');
		$res = $ins->execute([$nom, $slug]);

		if($res) {
			$message_categorie = 'La nouvelle catégorie a bien été ajoutée !';
		} else {
			$message_categorie = 'Une erreur est survenue.';
		}

	} else {
		$message_categorie = 'Tous les champs doivent être remplis';
	}
}

$categories = $bdd->query('SELECT * FROM categories');

$message_article = '';
if(isset($_POST['article'])) {
	if(isset($_POST['categorie_article'], $_POST['titre'], $_POST['contenu'])) {
		$categorie = htmlspecialchars($_POST['categorie_article']);
		$titre = htmlspecialchars($_POST['titre']);
		$contenu = htmlspecialchars($_POST['contenu']);
		

		if(!empty($categorie) AND !empty($titre) AND !empty($contenu)) {

			

				

					$ins = $bdd->prepare('INSERT INTO articles (titre, categorie, contenu, datetime_post) VALUES (:titre, :categorie, :contenu, NOW())');
					$res = $ins->execute([
							':titre' => $titre,
							':categorie' => $categorie,
							':contenu' => $contenu
						]);

					if($res) {
						$last_id = $bdd->lastInsertId();{
							$message_article = 'Votre article a bien été créé !';
						}

					} else {
						$message_article = 'Une erreur est survenue durant l\'ajout de votre article';
					}

				} 
		}

	 else {
		$message_article = 'Veuillez remplir tous les champs';
	}
}

$message_suppr = '';
if(isset($_POST['supprimer_article'])) {
	if(!empty($_POST['id_article'])) {
		$id_article = htmlspecialchars($_POST['id_article']);

		$suppr = $bdd->prepare('DELETE FROM articles WHERE id = ?');
		$res = $suppr->execute([$id_article]);

		if($res) {
			$message_suppr = 'Votre article a bien été supprimé';
		} else {
			$message_suppr = 'Une erreur est survenue durant la suppression de votre article';
		}
	} else {
		$message_suppr = 'Veuillez préciser l\'id de votre article';
	}
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Administration</title>
    <link rel="stylesheet" href="style.css">
    <script src="script/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="script/script.js" type="text/javascript"></script>
    <script src="script/jscolor.js"></script>
</head>
<header>
    
        <div class="container">
            <h1> <a href="index.php"> PhortNot</a></h1>
            <nav class="nav-wrap">
                <ul class="group" id="menu">
                <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
                <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
                <li><a rel="#98CEAA" href="profil.php">profil</a></li> 
                <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                <li><a rel="#D40229" href="#">STATS</a></li>
                <li class="current_page_item_two"><a rel="#900" href="/">BLOG</a></li>
                
               
        
        </ul>
        </nav>

</header>
<body>
    <div class="blog">

    <nav>
        <ul>
            <li><a href="blog.php">Acceuil</a></li>
            <li><a href="connexion-admin.php">Connexion</a> </li>
            <li><a href="admin.php">Administration</a> </li>
        </ul>
        </nav>
        <h2>Administration</h2>
    <section class="sidebar">
            <form action="">
                <input type="text" name="" id="recherche">
                <input type="submit" name="" id="recherche" value="Rechercher">
            </form>
            <h5>Catégories</h5>
            <ul>
            <?php while($c = $side_categories->fetch(PDO::FETCH_ASSOC)) { ?>
                <li><a href="blog.php?categorie=<?= $c['categorie_url'] ?>"><?= $c['categorie'] ?></a></li>
                <?php } ?>
            </ul>
            <h5>Commentaires récents</h5>
            <ul>
                <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
            </ul>
            
       </section>
    
        <a id="deconnect" href="blog.php">Se déconnecter</a>
        <h3>Nouvelle catégorie</h3>
        <form class="admin" method="POST">
            <input type="text" name="nom" placeholder="Nom de la catégorie" required>
            <input type="text" name="slug" size="30" placeholder="Slug de la catégorie (dans l'url)" required>
            <input type="submit" name="categorie" value="Créer la catégorie">
        </form>
        <?php if($message_categorie) { echo '<p>'.$message_categorie.'</p>'; } ?>


        <h3>Rédiger un article</h3>
        <form class="add" method="POST">
        <select name="categorie_article" required>
        <?php while($o = $categories->fetch(PDO::FETCH_ASSOC)) { ?>
            <option value="<?= $o['categorie_url'] ?>"<?php if(isset($categorie) AND $categorie == $o['categorie_url']) { echo ' selected'; } ?>><?= $o['categorie'] ?></option>
        <?php } ?>
	    </select>
            <br>
            <input type="text" name="titre" placeholder="Titre de l'article" <?php if(isset($titre)) { echo 'value="'.$titre.'"'; } ?> required>
            <br>
            <textarea name="contenu" placeholder="Contenu de l'article" style="width:60%;" required><?php if(isset($contenu)) { echo $contenu; } ?></textarea>
            <br>
            <input type="submit" value="Publier l'article" name="article">
        </form>
        <?php if($message_article) { echo '<p>'.$message_article.'</p>'; } ?>

        <br><br>

        <h3>Supprimer un article</h3>
        <form class="add" method="POST">
            <input type="number" name="id_article" placeholder="ID de l'article à supprimer" required>
            <br>
            <input type="submit" name="supprimer_article" value="Supprimer l'article">
        </form>
        <?php if($message_suppr) { echo '<p>'.$message_suppr.'</p>'; } ?>


        </div>
      
     
</body>
</html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';

