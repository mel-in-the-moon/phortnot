<?php
require_once 'functions/config.php';
require_once 'functions/functions.php';


$info_utilisateur = '';


$articles = $bdd->query('SELECT *, DATE_FORMAT(datetime_post, "%d %M %Y") AS date_formatee FROM articles ORDER BY datetime_post DESC');

if(!empty($_GET['categorie'])) {

	$get_categorie = htmlspecialchars($_GET['categorie']);
	$nom_categorie = getNomCategorie($get_categorie);

	$info_utilisateur = "Catégorie ".$nom_categorie;

	if(!$nom_categorie) {
		header('Location: blog.php');
		exit();
    }
    
    $articles = $bdd->prepare('SELECT *, DATE_FORMAT(datetime_post, "%d %M %Y") date_formatee FROM articles WHERE categorie = ? ORDER BY datetime_post DESC');
    $articles->execute([$get_categorie]);
}


?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php if($info_utilisateur) { echo $info_utilisateur.' - '; } ?>Blog</title>
    <link rel="stylesheet" href="style.css">
    <script src="script/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="script/script.js" type="text/javascript"></script>
    <script src="script/jscolor.js"></script>
</head>
<header>
    
        <div class="container">
            <h1> <a href="index.php"> PhortNot</a></h1>
            <nav class="nav-wrap">
                <ul class="group" id="menu">
                <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
                <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
                <li><a rel="#98CEAA" href="profil.php">profil</a></li> 
                <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                <li><a rel="#D40229" href="#">STATS</a></li>
                <li class="current_page_item_two"><a rel="#900" href="/">BLOG</a></li>
                
               
        
        </ul>
        </nav>

</header>
<body>
   
    <div class="blog">
        <nav>
        <ul>
            <li><a href="blog.php">Accueil</a></li>
            <li><a href="connexion-admin.php">Connexion</a> </li>
            <li><a href="admin.php">Administration</a> </li>
           
        </ul>
        </nav>
       
       <section class="articles main">
       <?php while($a = $articles->fetch(PDO::FETCH_ASSOC)) { ?>
       
        <div class="article" id="accueil">
            <img src="https://loremflickr.com/240/150" alt="thumb">
            <h3><a href="article.php?id=<?= $a['id'] ?>"> <?= $a['titre'] ?> </a></h3>
            <span class="categorie"><?= getNomCategorie($a['categorie']) ?></span> - <span class="date"><?= $a['date_formatee'] ?></span>
            <p><?= substr($a['contenu'], 0, 100).'...' ?></p>
        </div>
       <?php } ?>
        
       </section>
       <section class="sidebar">
            <form action="">
                <input type="text" name="" id="recherche">
                <input type="submit" name="" id="recherche" value="Rechercher">
            </form>
            <h5>Catégories</h5>
            <ul>
                <?php while($c = $side_categories->fetch(PDO::FETCH_ASSOC)) { ?>
                <li><a href="blog.php?categorie=<?= $c['categorie_url'] ?>"><?= $c['categorie'] ?></a></li>
                <?php } ?>
                
            </ul>
            <h5>Commentaires récents</h5>
            <ul>
                <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
                 <a href="#">Titre de l'article</a>
                 <li><i>Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Possimus, unde...</i></li><br>
            </ul>
            
       </section>
        
    </div>
  
      
      
     
</body>
</html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';

