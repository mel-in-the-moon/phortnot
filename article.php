<?php 
require_once 'functions/config.php';
require_once 'functions/functions.php';

if(!empty($_GET['id'])){
    $id = htmlspecialchars($_GET['id']);

    $article = $bdd->prepare('SELECT *, DATE_FORMAT(datetime_post, "%d %M %Y") date_formatee FROM articles WHERE id = ?');
    $article->execute([$id]);

    $article = $article->fetch(PDO::FETCH_ASSOC);

}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Article</title>
    <link rel="stylesheet" href="style.css">
    <script src="script/jquery-3.2.1.min.js" type="text/javascript"></script>
    <script src="script/script.js" type="text/javascript"></script>
    <script src="script/jscolor.js"></script>
</head>
<body>
    <header>
    <div class="container">
            <h1> <a href="index.php"> PhortNot</a></h1>
            <nav class="nav-wrap">
                <ul class="group" id="menu">
                <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
                <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
                <li><a rel="#98CEAA" href="profil.php">profil</a></li> 
                <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                <li><a rel="#D40229" href="#">STATS</a></li>
                <li class="current_page_item_two"><a rel="#900" href="blog.php">BLOG</a></li>
        
        </ul>
        </nav>
    </header>
    
    <div class="blog" id="unique">
    <section class="articles main">
    <nav>
        <ul>
            <li><a href="blog.php">Accueil</a></li>
            <li><a href="connexion-admin.php">Connexion</a> </li>
            <li><a href="admin.php">Administration</a> </li>
        </ul>
        </nav>
   
        <img class="img-article" src="http://placeimg.com/480/260/any" alt="thumb">
        <h3 id="article"><?= $article['titre'] ?></h3>
        <span class="categorie"><?= getNomCategorie($article['categorie']) ?></span> - <span class="date"><?= $article['date_formatee'] ?></span>
        <p><?= $article['contenu'] ?> </p> 
    </section>       
    
    <section class="sidebar">
            <form action="">
                <input type="text" name="" id="recherche">
                <input type="submit" name="" id="recherche" value="Rechercher">
            </form>
            <h5>Catégories</h5>
            <ul>
            <?php while($c = $side_categories->fetch(PDO::FETCH_ASSOC)) { ?>
                <li><a href="blog.php?categorie=<?= $c['categorie_url'] ?>"><?= $c['categorie'] ?></a></li>
                <?php } ?>
            </ul>
           
       </section>
       </div>
</body>
</html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';
?>