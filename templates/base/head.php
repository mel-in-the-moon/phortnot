<?php session_start(); ?>
<!DOCTYPE html>
<html>
    <head>
        
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
        <title>Tournois Phortnot</title>
        <link rel="stylesheet" href="style.css?1" type="text/css">
        <script src="script/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="script/script.js" type="text/javascript"></script>
        <script src="script/jscolor.js"></script>
    </head>
    <body>