<!DOCTYPE html>
<html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/head.php';
?>
    <body>
        <header>
            
                <div class="container">
                <h1> <a href="index.php"> PhortNot</a></h1>
                    
                <nav class="nav-wrap">
                    <ul class="group" id="menu">
                        <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
                        <li class="current_page_item_two"><a rel="#900" href="/">TOURNOI</a></li>
                        <li><a rel="#C6AA01" href="profil.php">PROFIL</a></li>
                        <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                        <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                        <li><a rel="#D40229" href="#">STATS</a></li>
                        <li><a rel="#98CEAA" href="blog.php">BLOG</a></li>
                        

                    </ul>
                </nav>

            
        </header>
      <section class="profil">
        <div class="text">
            <h2>Le tournoi Phortnot</h2>
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Tempora nobis fugiat sed distinctio optio libero eveniet expedita, excepturi aspernatur quae at aliquid obcaecati, animi et labore atque soluta, eum adipisci odio officiis. Consequatur, commodi repellat atque facere excepturi distinctio voluptas error amet quisquam dolor ex sapiente numquam, incidunt quos nobis mollitia impedit? Doloremque repellat quidem explicabo placeat soluta commodi quos iure, ullam tenetur? Ullam, dolore animi! Suscipit sit exercitationem minima!</p>
            <img src="img/ghostrunner.jpg" alt="">
        </div>
        <div class="text">
            <h2>Les duels</h2>
            <p>Pour vous inscrire: <a href="index.php">Retour à l'accueil</a></p>
            <ul>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quam, tempora!</li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sapiente corporis fugiat unde?</li>
                <li> Facilis quia voluptatem inventore neque ratione!</li>
                <li>Lorem ipsum dolor sit amet consectetur adipisicing elit. Ut, provident. Amet totam dolor repellat labore porro eveniet iusto corporis praesentium.</li>
                <li><img src="img/surgeCity.jpg" alt="">
</li>
            </ul>
            
        </div>
      </section>
      <section>
          
      </section>
       
        
    
    </body>
    
</html>
<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';
