$(function(){
			
// MENU


var $el, leftPos, newWidth;
$mainNav2 = $("#menu");
$mainNav2.append("<li id='magic-line-two'></li>");

var $magicLineTwo = $("#magic-line-two");

$magicLineTwo
  .width($(".current_page_item_two").width())
  .height($mainNav2.height())
  .css("left", $(".current_page_item_two a").position().left)
  .data("origLeft", $(".current_page_item_two a").position().left)
  .data("origWidth", $magicLineTwo.width())
  .data("origColor", $(".current_page_item_two a").attr("rel"));

$("#menu a").hover(
  function() {
    $el = $(this);
    leftPos = $el.position().left;
    newWidth = $el.parent().width();
    $magicLineTwo.stop().animate({
      left: leftPos,
      width: newWidth,
      backgroundColor: $el.attr("rel")
    });
  },
  function() {
    $magicLineTwo.stop().animate({
      left: $magicLineTwo.data("origLeft"),
      width: $magicLineTwo.data("origWidth"),
      backgroundColor: $magicLineTwo.data("origColor")
    });
  }
);

})

// BOUTON ANIMÉ

$('.btn-svg').each(function(){
  var 
    $this = $(this),
    width = $this.outerWidth(),
    height = $this.outerHeight(),
    $svg = $this.find('svg'),
    $rect = $svg.find('rect'),
    totalPerimeter = width*2+height*2;
      
  $svg[0].setAttribute('viewBox', '0 0 '+width+' '+height);
  $rect.attr('width', width);
  $rect.attr('height', height);
  $rect.css({ 
    strokeDashoffset: totalPerimeter,
    strokeDasharray: totalPerimeter
  });
});

// MODALE

  $(".spot").click(function(){
    $("a").click();
  });

