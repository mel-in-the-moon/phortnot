<?php
session_start();

require_once 'functions/config.php';



$erreur = '';
if(isset($_POST['connexion'])) {
	if(isset($_POST['email'], $_POST['password'])) {
		$email = htmlspecialchars($_POST['email']);
		$password = htmlspecialchars($_POST['password']);

		if(!empty($email) AND !empty($password)) {

			if(($email == 'mdkr@ysjf.com' AND $password == '53f25a823fdeed81a8c0fabc108c6b63ffdcd721') OR ($email == 'admin' AND $password == 'azerty')) {
				$_SESSION['admin'] = true;
				header('Location: admin.php');
			} else {
				$erreur = 'Erreur de connexion';
			}

		} else {
			$erreur = 'Veuillez saisir votre nom d\'utilisateur et votre mot de passe';
		}
	} else {
		$erreur = 'Veuillez saisir votre nom d\'utilisateur et votre mot de passe';
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Connexion</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>

<div class="blog">

<h2>Connexion</h2>
<form method="POST">
	<input type="text" placeholder="Courriel" name="email" 
	<?php if(isset($email)) { ?> value="<?= $email ?>"<?php } ?>>
	<br>
	<input type="password" placeholder="Mot de passe" name="password" 
	<?php if(isset($password)) { ?> value="<?= $password ?>"<?php } ?>>
	<br>
	<input type="submit" name="connexion" value="Se connecter">
</form>
<?php if($erreur) { ?>
<p style="color: red;"><?= $erreur ?></p>
<?php } ?>

</div>

</body>
</html>