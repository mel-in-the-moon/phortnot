<?php 
// include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/validation.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/user.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/head.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/connect.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/functions.php';

$mail = htmlspecialchars($_POST["mailconnect"]);


$user = get_user_by_email($mail);

?>
        <header>
         
                <div class="container">
                <h1> <a href="index.php"> PhortNot</a></h1>
                <nav class="nav-wrap">
                    <ul class="group" id="menu">
                    <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
                    <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
                    <li class="current_page_item_two"><a rel="#900" href="/">PROFIL</a></li>
                    <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
                    <li><a rel="#C6AA01" href="phortcare.php">PHORT-CARE</a></li>
                    <li><a rel="#D40229" href="#">STATS</a></li>
                    <li><a rel="#98CEAA" href="blog.php">BLOG</a></li>
            
            </ul>
            </nav>

            
        </header>
       <section class="profile">
            <div class="image">
                <h2>Mon profil</h2>
                <img src="img/user.jpg" alt="user">
               
            </div>
            <span class="info">
                    
                    <ul id="infos">
                    <li>Nom: <?php echo $user["nom"]; ?></li>
                    <li>Prenom: <?php echo $user["prenom"]; ?></li>
                    <li>Âge: <?php echo $user["date_de_naissance"]; ?></li>
                    <li>Numéro de tel: <?php echo $user["telephone"]; ?> </li>
                     <li>Courriel: <?php echo $user["email"]; ?> </li>
                </ul> 
                <?php
                 if(isset($_SESSION['id']) AND $user['id'] == $_SESSION['id']){
                ?>
                <?php
                 }
                 ?>
            <div class="edit">
                <a href="">Modifier mon profil</a>
                <a href="/functions/deconnexion.php">Déconnexion</a>
            </div>
            </span>
            <div class="text-profile">
            <button id="picture"> Modifier la photo</button>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil magni error nesciunt nobis modi ex tempore recusandae culpa, quod eum perferendis rerum maiores perspiciatis laborum nam fuga? Voluptate architecto id ipsa fuga natus odit enim, provident in. Doloribus neque, nesciunt ad reprehenderit blanditiis illum illo debitis nulla alias, laboriosam iste.</p>

                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Nihil magni error nesciunt nobis modi ex tempore recusandae culpa, quod eum perferendis rerum maiores perspiciatis laborum nam fuga? Voluptate architecto id ipsa fuga natus odit enim, provident in. Doloribus neque, nesciunt ad reprehenderit blanditiis illum illo debitis nulla alias, laboriosam iste.</p>
            </div>
       </section>
       
        
       <?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';
?>
