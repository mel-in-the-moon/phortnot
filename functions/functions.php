<?php 


function connect(){
    try{
        return new  PDO("mysql:host=127.0.0.1;dbname=phortnot_mlandry","root","");
    }catch( PDOException $e ){
        var_dump($e);
    } 
}

function debug_pdo($query){
    echo "<pre>";
    print_r($query->errorInfo());
    $query->debugDumpParams();
    echo "</pre>";
    var_dump($query);
}

function getNomCategorie($slug){
    global $bdd;

    $categorie = $bdd->prepare('SELECT categorie FROM categories WHERE categorie_url = ?');
    $categorie->execute([$slug]);

    $categorie = $categorie->fetchColumn();
    return $categorie;
}