<?php

function validate_form($form_data)
{
    $results = [];
   
    

    foreach($form_data as $key =>$data){
        $validate = validate($key, filter_var($data, FILTER_SANITIZE_STRING));
        if(strlen($validate) > 0){
            $results[$key] = $validate;
        }
    }
    return $results;
}

function validate($key, $data){
    $noms = ["nom", "prenom", "birth", "tel", "password"];
    $courriel = ["courriel"];
    $phone = ["tel"];

    if(in_array($key,$noms)){
        if(empty($data)){
            return "Le champ $data ne doit pas etre vide.";
        }
    } else if (in_array($key, $courriel)){
        if(!filter_var(trim($data), FILTER_VALIDATE_EMAIL)){
            return "Le champ $key n'est pas rempli correctement";
        }
    }
}