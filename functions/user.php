<?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/functions.php';
// include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/validation.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/functions/router.php';


$erreur = '';
if (isset($_POST["forminscription"])) {
    $mail = htmlspecialchars($_POST["mailconnect"]);
    $passwordconnect = sha1($_POST["password"]);}

$bdd = connect();

function get_users(){
    $bdd = connect();
   
    try{
        $requsers= $bdd->prepare("SELECT * FROM user");
        $requsers->execute();
        return $requsers->fetchAll();
    } catch(PDOException $e){
        echo $e->getMessage();
    }
    $bdd= null;
}

function get_user_by_email($mail){
    $bdd = connect();
    
    try{
        $requsers= $bdd->prepare("SELECT * FROM user WHERE email = :mail");
        $requsers->execute([":mail"=>$mail]);
        return $requsers->fetch();
    } catch(PDOException $e){
        echo $e->getMessage();
    }
    $bdd= null;
}

function validerCourriel($mail){
    $bdd = connect();

    try{
        $reqmail = $bdd->prepare(("SELECT * FROM user WHERE email = ?"));
        $reqmail->execute((array($mail)));
        $mailexist = $reqmail->rowCount();
        return $mailexist;
    } catch(PDOException $e){
        echo $e->getMessage();
    }

}

function traiter_inscription($data){
    $bdd = connect();
    $nom = htmlspecialchars($data["nom"]);
    $prenom = htmlspecialchars($data["prenom"]);
    $date = htmlspecialchars($data["birth"]);
    $phone = htmlspecialchars($data["tel"]);
    $mail = htmlspecialchars($data["courriel"]);
    $password = sha1($data["password"]);

    if (!empty($data["nom"]) and !empty($data["prenom"]) and !empty($data["birth"]) and !empty($data["tel"]) and !empty($data["password"]) and  !empty($data["nom"])) {
        if (filter_var(trim($data["courriel"]), FILTER_VALIDATE_EMAIL) == $data['courriel']) {
            $mailexist = validerCourriel($data['courriel']);
         
            if ($mailexist !== 0) {
                return ["error"=>"Ce courriel est déjà utilisé"];
              $errors = "L'adresse email est déjà utilisée";
            } else {
                try {
                  $insertuser = $bdd->prepare("INSERT INTO user(prenom,nom,date_de_naissance,telephone,email,mot_de_passe) 
                                               VALUES(:prenom,:nom,:date_de_naissance,:telephone,:email,:mot_de_passe)");
                  $insertuser->execute([
                    ":prenom" => $prenom,
                    ":nom" => $nom,
                    ":date_de_naissance" => $date,
                    ":telephone" => $phone,
                    ":email" => $mail,
                    ":mot_de_passe" => $password
                  ]);

                } catch (PDOException $e) {
                    echo $e->getMessage();
                }
                $_SESSION["comptecree"] = "Votre compte a bien été crée";
                header("Location:../profil.php");
            }
        }
    }
}

function get_champions(){
    $bdd = connect();
    
    
    /*Select de tout les utilisateur ( dsauf ceux qui sont morts ) */
    /** Return de la liste des utilisateurs */
    try{
        $requsers= $bdd->prepare("SELECT * FROM user WHERE mort =1");
        $requsers->execute();
        return $requsers->fetchAll();
    } catch(PDOException $e){
        echo $e->getMessage();
    }
    $bdd= null;
}

 if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $errors = '';
    $errors = [];

    if (isset($_POST["forminscription"])) {
       
        $nom = htmlspecialchars($_POST["nom"]);
        $prenom = htmlspecialchars($_POST["prenom"]);
        $date = htmlspecialchars($_POST["birth"]);
        $phone = htmlspecialchars($_POST["tel"]);
        $mail = htmlspecialchars($_POST["mailconnect"]);
        $password = sha1($_POST["password"]);

        if (!empty($_POST["nom"]) and !empty($_POST["prenom"]) and !empty($_POST["birth"]) and !empty($_POST["tel"]) and !empty($_POST["password"]) and  !empty($_POST["nom"])) {
            if (filter_var(trim($_POST["mailconnect"]), FILTER_VALIDATE_EMAIL) == $_POST['mailconnect']) {
                $traitement = traiter_inscription($_POST);
                if (is_null($traitement[$errors])){
                    header("Location: ../profil.php");
                } else {
                    array_push($errors, $traitement[$errors]);
                }
               try{
                   $reqmail = $bdd->prepare(("SELECT * FROM user WHERE email = ?"));
                   $reqmail->execute((array($mail)));
                    
                   $mailexist = $reqmail->rowCount();
                   debug_pdo($reqmail);
               } catch(PDOException $e){
                   echo $e->getMessage();
               }
             
               if ($mailexist !== 0) {
                   $errors = "L'adresse email est déjà utilisée";
                   var_dump("test2");
               } else {
                   try {
                     $insertuser = $bdd->prepare("INSERT INTO user(prenom,nom,date_de_naissance,telephone,email,mot_de_passe) 
                                                  VALUES(:prenom,:nom,:date_de_naissance,:telephone,:email,:mot_de_passe)");
                     $query = $insertuser->execute([
                       ":prenom" => $prenom,
                       ":nom" => $nom,
                       ":date_de_naissance" => $date,
                       ":telephone" => $phone,
                       ":email" => $mail,
                       ":mot_de_passe" => $password
                     ]);

                   } catch (PDOException $e) {
                       echo $e->getMessage();
                   }
                   $_SESSION["comptecree"] = "Votre compte a bien été crée";
           
               }
            }
        }
    }
}
?>
    

