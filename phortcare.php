<!DOCTYPE html>
<html>
<head>
        
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="https://fonts.googleapis.com/css2?family=Josefin+Sans&display=swap" rel="stylesheet">
<title>boutique  phortcare </title>
<link rel="stylesheet" href="style.css?1" type="text/css">
<script src="script/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="script/script.js" type="text/javascript"></script>
<script src="script/jscolor.js"></script>
</head>
<header id="phortcare">
<p>phortcare</p>
    <nav class="nav-wrap">
    <ul class="group" id="menu">
        <li><a rel="#fe4902" href="index.php">ACCUEIL</a></li>
        <li><a rel="#A41322" href="tournoi.php">TOURNOI</a></li>
        <li><a rel="#C6AA01" href="profil.php">PROFIL</a></li>
        <li><a rel="#C6AA01" href="champions.php">CHAMPIONS</a></li>
        <li class="current_page_item_two"><a rel="#900" href="/">PHORT-CARE</a></li>
        <li><a rel="#D40229" href="#">STATS</a></li>
        <li><a rel="#98CEAA" href="blog.php">BLOG</a></li>
    
    </ul>
    </nav>
    
</header>
<body>
<section id="store">

    <h1 class="subtitle">La boutique</h1>
    <div id="gallery">

        <h3>La collection d'armes</h3>
        <aside>
        <div id="workarea">
            <div class="position">
            <div class="svg-wrapper">
            <svg height="40" width="150" xmlns="http://www.w3.org/2000/svg">
            <rect id="shape" height="40" width="150" />
            <div id="text">
            <a onclick="alert('Tout achat est lié à la signature d\’un contrat de non responsabilité qui dégage la boutique de toute responsabilité liée à l\’utilisation des armes.')"><span class="spot"></span>AVERTISSEMENT</a>
            </div>
            </svg>
    </div>
  
        </aside>
                
            <div id="galery-big">

                <h4>WTA - C. Blade</h4>
                <img class="armes" src="img/big/a.jpg" alt="image1"> 
                
            </div>
            <div class="wp">
            <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id officia et esse repudiandae doloribus ad, ipsam ab soluta 
                    eveniet ex suscipit explicabo possimus pariatur quisquam omnis! Velit soluta culpa est reprehenderit, ullam adipisci architecto 
                    at rem neque, a delectus eum expedita quisquam et, laudantium unde tempora dolore esse veniam. Laborum quaerat qui optio autem 
                    iure non fugit molestias voluptate dolorum. Sunt, qui recusandae at quo quasi asperiores numquam vitae similique? Totam quibusdam magnam similique aspernatur?</p>
                    <br><br><br><br><br>
                    <p id="prix">2098$</p>
                    <input type="submit" class="acheter" value="Acheter">
            </div>
                    <div id="galery-nav">

                        <a rel="img1" href=""><img src="img/small/arme1.png" alt=""> </a>
                        <a rel="img2" href=""><img src="img/small/arme2.png" alt=""> </a>
                        <a rel="img3" href=""><img src="img/small/arme3.png" alt=""> </a>
                        <a rel="img4" href=""><img src="img/small/arme4.png" alt=""> </a>
                        <a rel="img5" href=""><img src="img/small/arme8.png" alt=""> </a>
                        <a rel="img6" href=""><img src="img/small/arme6.png" alt=""> </a>
                        <a rel="img7" href=""><img src="img/small/arme7.png" alt=""> </a>
                    </div>  
                    <div id="galery-armures">
                        <div class="line">

                        </div>
                        <h4>Animal Yrt-8</h4>
                        <img class="armures" src="img/Badmustache.jpg" alt="">
                        <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Id officia et esse repudiandae doloribus ad, ipsam ab soluta 
                            eveniet ex suscipit explicabo possimus pariatur quisquam omnis! Velit soluta culpa est reprehenderit, ullam adipisci architecto 
                            at rem neque, a delectus eum expedita quisquam et, laudantium unde tempora dolore esse veniam. Laborum quaerat qui optio autem 
                            iure non fugit molestias voluptate dolorum. Sunt, qui recusandae at quo quasi asperiores numquam vitae similique? Totam quibusdam magnam similique aspernatur?</p>
                            <br><br><br><br><br>
                    </div>    
                    
        </div>
        

    </section>
</body>
    <?php
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/footer.php';
include_once $_SERVER['DOCUMENT_ROOT'] . '/templates/base/foot.php';
